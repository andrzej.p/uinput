/*
 * Example uinput device.
 *
 * (C) Collabora 2020
 * Andrzej Pietrasiewicz <andrzej.p@collabora.com>
 *
 * Based on Documentation/input/uinput.rst in the kernel source tree.
 * (https://www.kernel.org/doc/html/latest/input/uinput.html)
 *
 * libevdev could have been used.
 *
 * Error handling omitted for brevity.
 */
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/uinput.h>

static struct uinput_setup mouse = {
	.id = {
		.bustype = BUS_USB,
		.vendor = 0x1234,	/* sample vendor */
		.product = 0x5678,	/* sample product */
	},
	.name = "Virtual mouse",
};

static int cont = 1;

static void sighandler(int sig)
{
	cont = 0;
}

static struct sigaction int_handler = {
	.sa_handler = sighandler,
};

static void input_event(int fd, int type, int code, int val)
{
	struct input_event ie;

	ie.type = type;
	ie.code = code;
	ie.value = val;
	/* timestamp values below are ignored */
	ie.time.tv_sec = 0;
	ie.time.tv_usec = 0;

	/*
	 * results in
	 * input_event(struct input_dev *dev, unsigned int type, unsigned int code, int value))
	 * inside the kernel
	 */
	write(fd, &ie, sizeof(ie));
}

static void mouse_move(int fd, int n_reports, int x_delta, int y_delta)
{
	int i;

	for (i = 0; i < n_reports; ++i) {
		input_event(fd, EV_REL, REL_X, x_delta );
		input_event(fd, EV_REL, REL_Y, y_delta);
		input_event(fd, EV_SYN, SYN_REPORT, 0);
		usleep(15000);
	}
}

int main(void)
{
	int fd;

	sigaction(SIGINT, &int_handler, NULL);

	fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);

	/*
	 * Declare relative events and mouse button left
	 * even though we only generate relative events.
	 * Otherwise the mouse handler will not listen to us.
	 */
	ioctl(fd, UI_SET_EVBIT, EV_KEY);
	ioctl(fd, UI_SET_KEYBIT, BTN_LEFT);

	ioctl(fd, UI_SET_EVBIT, EV_REL);
	ioctl(fd, UI_SET_RELBIT, REL_X);
	ioctl(fd, UI_SET_RELBIT, REL_Y);

	ioctl(fd, UI_DEV_SETUP, &mouse);
	ioctl(fd, UI_DEV_CREATE);
	printf("Device created\n");

	/* let the system notice the new device */
	sleep(1);
	printf("Starting generating events\n");

	/* keep moving along a rhombus */
	while (cont) {
		mouse_move(fd, 50, 5, 5);
		mouse_move(fd, 50, 5, -5);
		mouse_move(fd, 50, -5, -5);
		mouse_move(fd, 50, -5, 5);
	}

	printf("Closing...");
	ioctl(fd, UI_DEV_DESTROY);
	close(fd);
	printf("OK\n");

	return 0;
}
